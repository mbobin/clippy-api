Rails.application.routes.draw do
  root "resources#index"
  
  resources :resources

  namespace :api do
    defaults format: :json do
      resources :resources, only: [:create, :show, :index]
    end
  end
end
