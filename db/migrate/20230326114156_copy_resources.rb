class CopyResources < ActiveRecord::Migration[7.0]
  def up
    execute(<<~SQL)
      insert into resources select * from text_resources;
    SQL
  end

  def down
  end
end
