class CreateTextResources < ActiveRecord::Migration[7.0]
  def change
    create_table :text_resources do |t|
      t.text :data

      t.timestamps
    end
  end
end
