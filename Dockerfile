FROM ruby:3.3.5-alpine
RUN apk add --no-cache --update \
  build-base \
   sqlite-dev

ENV APP_PATH=/var/apps/clippy
ENV RAILS_ENV=production

WORKDIR $APP_PATH
ADD Gemfile $APP_PATH
ADD Gemfile.lock $APP_PATH
RUN bundle config set --local without 'test development' && \
  bundle config set --local deployment 'true' && \
  bundle install --jobs `expr $(cat /proc/cpuinfo | grep -c "cpu cores") - 1` --retry 3

COPY . $APP_PATH
RUN SECRET_KEY_BASE=$(bin/rails secret) bin/rails assets:precompile 

EXPOSE 3000

CMD ["bin/server"]
