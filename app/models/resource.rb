require 'net/http'
require 'uri'

class Resource < ApplicationRecord
  has_one_attached :file
  validates :data, presence: true

  after_commit :enqueue_video_processing, on: :create

  def enqueue_video_processing
    downloads_api = ENV['YOUTUBE_API']
    return unless downloads_api.present?
    return unless data.to_s.match?(/youtube\.com|youtu\.be/)

    url = data.split(/\s+/).find { |string| string.start_with?(/https\:\/\/w{0,3}\.?youtu/) }
    return unless url.present?

    Thread.new do
      Net::HTTP.post(
        URI(downloads_api),
        { 'url' => url }.to_json,
        "Content-Type" => "application/json"
      )
    end
  end
end
