module Api
  class ResourcesController < ApiController
    before_action :set_resource, only: %i[ show ]

    # GET /resources
    def index
      resources = Resource.all.order(id: :desc).includes(file_attachment: :blob)
      render json: resources.to_json(include: { file: { only: [], methods: [:url] } })
    end

    def show
      render plain: @resource.data
    end

    def create
      @resource = Resource.new(resource_params)

      if @resource.save
        render plain: "Resource was successfully created, id: #{@resource.id}"
      else
        render plain: @resource.errors.full_messages.join(', ')
      end
    end

    private
      def set_resource
        @resource = Resource.find(params[:id])
      end

      def resource_params
        params.permit(:data, :file)
      end
  end
end
