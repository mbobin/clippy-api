module Api
  class ApiController < ActionController::Base
    include ActiveStorage::SetCurrent

    protect_from_forgery with: :null_session
  end
end
